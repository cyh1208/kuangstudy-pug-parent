package com.ksd.pug.service.role;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ksd.pug.pojo.SysRole;

/**
 * Created by yykk on 17/1/20.
 */
public interface ISysRoleService extends IService<SysRole> {
}
