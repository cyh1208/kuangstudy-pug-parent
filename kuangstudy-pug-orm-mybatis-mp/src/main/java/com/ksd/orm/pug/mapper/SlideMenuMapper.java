package com.ksd.orm.pug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ksd.pug.pojo.SlideMenu;

/**
 * @author 飞哥
 * @SysLoginUseritle: 学相伴出品
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/1/2 12:42
 */
public interface SlideMenuMapper extends BaseMapper<SlideMenu> {
}
