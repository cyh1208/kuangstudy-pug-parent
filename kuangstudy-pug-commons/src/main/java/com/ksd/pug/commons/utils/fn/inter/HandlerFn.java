package com.ksd.pug.commons.utils.fn.inter;

@FunctionalInterface
public interface HandlerFn {

    void handler();
}
