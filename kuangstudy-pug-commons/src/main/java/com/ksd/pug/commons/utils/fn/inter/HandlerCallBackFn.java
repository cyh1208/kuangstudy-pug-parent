package com.ksd.pug.commons.utils.fn.inter;

@FunctionalInterface
public interface HandlerCallBackFn<T> {

    T handler();
}
