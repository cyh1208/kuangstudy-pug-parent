package com.ksd.pug.commons.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/1/6 16:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserVo implements java.io.Serializable {
    private Long userId;
    private String token;
}
