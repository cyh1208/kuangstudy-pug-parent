package com.ksd.pug.config.webmvc;

import com.ksd.pug.config.interceptor.jwt.JwtInterceptor;
import com.ksd.pug.config.interceptor.repeat.RepeatSubmitInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/1/6 17:50
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    /**
     * jwt的token校验
     */
    @Autowired
    public JwtInterceptor jwtInterceptor;

    /**
     * 表单重复提交
     */
    @Autowired
    private RepeatSubmitInterceptor repeatSubmitInterceptor;

    /**
     * 拦截的注册
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(repeatSubmitInterceptor).addPathPatterns("/admin/v1/**");
        registry.addInterceptor(jwtInterceptor).addPathPatterns("/admin/v1/**");
    }
}
