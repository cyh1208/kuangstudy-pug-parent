package com.ksd.pug.generator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 飞哥B站地址：https://space.bilibili.com/490711252
 * 记得关注和三连哦！
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2022/1/4 23:57
 */
public class MybatisPlusGenerator {

    public static void main(String[] args) {
        FastAutoGenerator
                .create("jdbc:mysql://127.0.0.1:3306/ksd-pug-travel?serverTimezone=GMT%2b8&useUnicode=true&characterEncoding=utf-8&useSSL=false", "root", "mkxiaoer")
                .globalConfig(builder -> {
                    builder.author("yykk") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("C:\\test"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.ksd.pug") // 设置父包名
                            .moduleName("") // 设置父包模块名
                            .entity("pojo")
                            .service("service")
                            .controller("controller")
                            .mapper("mapper")
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "C:\\test")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("kss_items") // 设置需要生成的表名
                            .addTablePrefix("kss_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
